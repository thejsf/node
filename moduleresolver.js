export class ModuleResolver {

    constructor(cache) {
        if (cache) {
            this.cache = cache;
        } else {
            this.cache = {};
        }
    }

    getModule(moduleName) {
        if (!this.cache[moduleName]) {
            this.cache[moduleName] = require('./' + moduleName);
        }
        return this.cache[moduleName];
    }

    setModule(moduleName, module) {
        if (this.cache[moduleName]) {
            throw new Error("This ModuleResolver does not support changing object type mappings that have already been initialized.");
        }
        this.cache[moduleName] = module;
    }
}
