/* nodeserver.js
 * The purpose of server.js is to receive, respond to, and route requests.
 */

import Express from 'express';
var logger = require('./logger');
var express = module.exports = Express();

function start(port) {

  const AppController = NodeServer.moduleResolver.getModule('controllers/app').AppController;
  const appController = new AppController();
  appController.addRoutes(express);

  express.listen(port);
  logger.info( 'nodeserver listening on port ' + port + '.');

}

module.exports.start = start;
