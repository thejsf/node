export class ViewInterface {
    constructor(model) {
        this.model = model;
    }
    render(res) {
        throw new Error("Base method ViewInterface.render cannot be invoked. Use a class that extends ViewInterface and implements the render(res) method.");
    }
}

