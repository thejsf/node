/*
 * server.js - main entry point for the application
 * The purpose of server.js is to read configuration from configuration sources
 * (defaults, command-line, options files, and so on)
 * and start the webServer.
 * here we are....
 */


// noinspection JSUndefinedPropertyAssignment
// noinspection JSUndefinedPropertyAssignment
global.NodeServer = {}; // global Object container

const util = require("util");
const logger = require("./logger");
const configger = require("./configger");
NodeServer.config  = configger.load({webServer: {port: 8080}});
NodeServer.packageJson = require('./package.json');

const ModuleResolver = require('./moduleresolver').ModuleResolver;
NodeServer.moduleResolver = new ModuleResolver();
NodeServer.views = {};

const webServer = NodeServer.moduleResolver.getModule('nodeserver');

// noinspection JSUnresolvedVariable
logger.addTargets(NodeServer.config.loggingTargets);

logger.info("app version: " + NodeServer.packageJson.version);
logger.debug("config: " + util.inspect(NodeServer.config, {depth: null}));
logger.debug("package.json: " + util.inspect(NodeServer.packageJson,{depth: null}));

webServer.start(NodeServer.config.webServer.port);
