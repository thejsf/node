var logger = NodeServer.moduleResolver.getModule('logger');

export class AppController {

    addRoutes(webServer) {
        webServer.get('/', function (req, res) {
            logger.debug("in app.get(). req.headers=", req.headers);

            function getViewNames() {
                if (!NodeServer.views['app']) {
                    NodeServer.views['app'] = ['application/json', 'text/csv'];
                }
                return NodeServer.views['app'];
            }

            function getViewName(res) {
                var s = res.get('Content-Type');
                return 'views/app/' + s.substring(0, s.indexOf(";")).toLowerCase();
            }

            var formatterObject = {
                'default': function () {
                    // log the request and respond with 406
                    res.status(406).send({AvailableMediaTypes: getViewNames()});
                }
            };

            var viewNames = getViewNames();

            for (var i = 0; i < viewNames.length; i++) {
                formatterObject[viewNames[i]] = function () {
                    const AppModel = NodeServer.moduleResolver.getModule('models/app').AppModel;
                    const appModel = new AppModel();
                    const View = NodeServer.moduleResolver.getModule(getViewName(res)).View;
                    const view = new View(appModel);
                    view.render(res);
                };
            }

            res.format(formatterObject)
        });

        webServer.options('/', function (req, res) {
            logger.debug("in app.options(). req.headers=", req.headers);
            res.format({
                'application/json': function () {
                    res.set('Allow', 'GET,OPTIONS').send('');
                },

                'default': function () {
                    // log the request and respond with 406
                    res.status(406).send({AvailableMediaTypes: ['application/json']});
                }
            })
        });

        webServer.post('/', function (req, res) {
            logger.debug("in app.post(). req.headers=", req.headers);
            res.status(405).send('POST not supported')
        });

        webServer.put('/', function (req, res) {
            logger.debug("in app.put(). req.headers=", req.headers);
            res.status(405).send('PUT not supported')
        });

        webServer.delete('/', function (req, res) {
            logger.debug("in app.del(). req.headers=", req.headers);
            res.status(405).send('DELETE not supported')
        });
    }
}
