export class AppModel {

    constructor() {
        if (!NodeServer.appModel) {
            this._type = 'AppModel';
            this._config = {
                name: NodeServer.packageJson.name,
                version: NodeServer.packageJson.version,
                port: NodeServer.config.http.port
            };
            NodeServer.appModel = this;
        }
        return NodeServer.appModel;
    }

    get config() {
        return this._config;
    }

    get type() {
        return this._type;
    }

}

