// noinspection JSUnresolvedVariable
const ViewInterface = NodeServer.moduleResolver.getModule('interfaces/view').ViewInterface;

export class View extends ViewInterface {

    render(res) {
        const result = 'name,version\n' + this.model.config.name + "," + this.model.config.version;
        res.send(result);
    };

}
