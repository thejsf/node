const ViewInterface = NodeServer.moduleResolver.getModule('interfaces/view').ViewInterface;

export class View extends ViewInterface {

    render(res) {
        var result = {name: this.model.config.name, version: this.model.config.version};
        res.json(result);
    }

}
