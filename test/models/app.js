const assert = require('assert');

require('../mocharootsuite');

describe('app model tests', function () {

    let AppModel;

    before(function() {
        AppModel = NodeServer.moduleResolver.getModule('models/app').AppModel;
        NodeServer.config = {http:{port:1338}};
        NodeServer.packageJson = {name: 'foo', version: 'bar'};
    });

    it('should return \"AppModel\" for the type property', function () {
        const appModel = new AppModel();
        assert.strictEqual(appModel.type, 'AppModel');
    });

    it('should return appropriate config on calls to getConfig',  function() {
        const appModel = new AppModel();
        assert.deepStrictEqual(appModel.config, {name: 'foo', version: 'bar', port: 1338});
    });

});
