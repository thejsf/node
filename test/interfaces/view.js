require('../mocharootsuite');

require('should');

describe('view interface tests', function () {

    it('should raise an error if you try to call render', function () {

        const View = NodeServer.moduleResolver.getModule('interfaces/view').ViewInterface;
        const view = new View({});

        var called = false;
        var mockResponse = {
            json: function () {
                called = true;
            }
        };

        (function(){
            view.render(mockResponse);
        }).should.throwError('Base method ViewInterface.render cannot be invoked. Use a class that extends ViewInterface and implements the render(res) method.');

    });
});
