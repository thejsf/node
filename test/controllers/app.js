var webServer;
var assert = require ('assert');
var request = require('supertest');
require("should");

require('../mocharootsuite');

describe('app controller tests', function () {

    var hitCounts = {
        jsonView: 0,
        csvView: 0,
    };


    beforeEach(function(){
        webServer = NodeServer.moduleResolver.getModule('nodeserver');
        webServer.listen = function(){};
        webServer.start(1338);
        class MockAppModel {
            static getConfig() {
                return {name: "Foo", version: "1.3.37", port: 1338};
            }
        }
        const ViewInterface = NodeServer.moduleResolver.getModule('interfaces/view').ViewInterface;
        class MockJsonView extends ViewInterface {
            render(res){
                hitCounts.jsonView = hitCounts.jsonView + 1;
                res.json({name: "Foo", version: "1.3.37", port: 1338});
            }
        }

        class MockCsvView extends ViewInterface {
            render(res){
                hitCounts.csvView = hitCounts.csvView + 1;
                res.send('name,version\nFoo,1.3.3.7');
            }
        }
        NodeServer.moduleResolver.setModule('models/app', {AppModel: MockAppModel});
        NodeServer.moduleResolver.setModule('views/app/application/json', {View: MockJsonView});
        NodeServer.moduleResolver.setModule('views/app/text/csv', {View: MockCsvView});
    });

    it('should have an index route accessible via GET', function (done) {
        request(webServer).get('/').expect(200, done);
    });

    it('should return JSON', function (done) {
        request(webServer).get('/').expect('Content-Type', /application\/json/, done);
    });

    it('should request the application_json view', function (done) {
        var beforeHits = hitCounts.jsonView;
        request(webServer).get('/').expect(function(){
            assert(hitCounts.jsonView > beforeHits);
        }).end(done);

    });

    it('should request the text_csv view', function (done) {
        var beforeHits = hitCounts.csvView;
        request(webServer)
            .get('/')
            .set('Accept', 'text/csv')
            .expect(function(){
                assert(hitCounts.csvView > beforeHits);
            })
            .end(done);

    });

    it('should return 406 if request.accepts is invalid', function(done){
        request(webServer).get('/').set('Accept', 'xyzzy/plugh').expect(406, done);
    });

    it('should return a body indicating that application/json is available if request.accepts is invalid', function(done){
        request(webServer).get('/').set('Accept', 'xyzzy/plugh').expect(function(res){
            res.body.should.have.property('AvailableMediaTypes').containEql('application/json')
        }).end(done);    });

    it('should return a body indicating that application/json is available if request.accepts is invalid on call to OPTIONS', function(done){
        request(webServer).options('/').set('Accept', 'xyzzy/plugh').expect(function(res){

            res.body.should.have.property('AvailableMediaTypes').containEql('application/json')
        }).end(done);    });

    it('should return 405 if POST is called', function(done){
        request(webServer).post('/', '').expect(405, done);
    });

    it('should return 405 if PUT is called', function(done){
        request(webServer).put('/', '').expect(405, done);
    });

    it('should return 405 if DELETE is called', function(done){
        request(webServer).delete('/').expect(405, done);
    });

    it('should return 200 if OPTIONS is called', function(done){
       request(webServer).options('/').expect(200, done);
    });

    it('should return GET,OPTIONS if OPTIONS is called', function(done){
        request(webServer).options('/').expect('Allow', 'GET,OPTIONS', done);
    });

    it('should return application/json if OPTIONS is called', function(done){
        request(webServer).options('/').expect('Content-Type',
            /application\/json/, done);
    });

});
