/*
 * server.js - main entry point for the application
 *
 * not tested: write some log messages
 *
 * not tested: call nodeServer.start with the correct port number
 */

//TODO test log messages
//TODO test call nodeServer.start with the correct port number
require('./mocharootsuite');
const assert = require('assert');

describe('server.js tests', function () {

    const dummy = { foo: 'bar' };

    before(function () {
        global.NodeServer = dummy;
        require('../server');
    });

    it('should overwrite the global NodeServer object', function () {
        assert.notStrictEqual(global.NodeServer, dummy);
    });

    it('should store an object in NodeServer.config', function() {
       assert.strictEqual(typeof NodeServer.config, 'object');
    });

    it('should store an object in NodeServer.packageJson', function() {
        assert.strictEqual(typeof NodeServer.packageJson, 'object');
    });

    it('should store the contents of package.json in NodeServer.packageJson', function(){
       assert.deepStrictEqual(NodeServer.packageJson, require('../package.json'));
    });

    it('should store the contents of config.json in NodeServer.config', function(){
        assert.deepStrictEqual(NodeServer.config.loggingTargets, require('../config.json').loggingTargets);
        assert.deepStrictEqual(NodeServer.config.webServer, require('../config.json').webServer);
    });

    it('should initialize the view cache', function() {
       assert.deepStrictEqual(NodeServer.views, {});
    });

    it('should store an object with the interface of a ModuleResolver in NodeServer.moduleResolver', function(){
        assert.strictEqual(typeof NodeServer.moduleResolver, 'object');
        assert.strictEqual(typeof NodeServer.moduleResolver.getModule, 'function');
        assert.strictEqual(typeof NodeServer.moduleResolver.setModule, 'function');
    });
});
