import { ModuleResolver }  from '../moduleresolver';

// noinspection JSUndefinedPropertyAssignment
global.NodeServer = {
   moduleResolver: new ModuleResolver()
};

beforeEach (function(){
   NodeServer.moduleResolver = new ModuleResolver();
   NodeServer.views = {};
});
