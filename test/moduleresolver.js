require ('./mocharootsuite');
import { ModuleResolver } from '../moduleresolver';

const assert = require('assert');

describe('moduleResolver tests', function () {

    let hitCount = 0;

    // noinspection JSUnresolvedVariable
    const ViewInterface = NodeServer.moduleResolver.getModule('interfaces/view').ViewInterface;

    class MockView extends ViewInterface {
        render(){
            hitCount = hitCount + 1;
        }
    }

    it('should be instantiatable', function () {
        // noinspection JSUnusedLocalSymbols
        const moduleResolver = new ModuleResolver();
    });

    it('should return something that can be used to construct a new object', function() {

        const moduleResolver = new ModuleResolver();
        const View = moduleResolver.getModule("views/app/application/json").View;
        // noinspection JSUnusedLocalSymbols
        const view = new View();

    });

    it ('should be mockable', function() {

        const moduleResolver = new ModuleResolver();


        moduleResolver.setModule('views/app/application/json', {View: MockView});

        const View = moduleResolver.getModule('views/app/application/json').View;
        const view = new View({name: 'foo', version:'bar', port:'1339'});
        const hitsBefore = hitCount;
        view.render({foo:'bar'});
        assert(hitCount = hitsBefore + 1);
    });

    it('should accept a cache in the constructor', function(){
        const cache = {'views/app/application/json': {View: MockView}};

        const moduleResolver = new ModuleResolver(cache);

        const View = moduleResolver.getModule('views/app/application/json').View;
        const view = new View({name: 'foo', version:'bar', port:'1339'});
        const hitsBefore = hitCount;
        view.render({foo:'bar'});
        assert(hitCount = hitsBefore + 1);

    });

    it('should throw an error if an attempt is made to overwrite a cached entry', function() {
        const moduleResolver = new ModuleResolver();
        // noinspection JSUnusedLocalSymbols
        const View = moduleResolver.getModule('views/app/application/json').View;

        // noinspection JSUnresolvedVariable
        (function(){
            moduleResolver.setModule('views/app/application/json', {View: MockView})
        }).should.throwError('This ModuleResolver does not support changing object type mappings that have already been initialized.');
    });

});
