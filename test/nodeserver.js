/*
node server does the following things:

export a start(port) function that:
    imports AppController from module AppController using the moduleresolver
    creates a new AppController
    calls AppController.addRoutes passing the express object
    calls express.listen using the passed port
    writes a log message
 */
require('./mocharootsuite');

import Express from 'express';
const assert = require('assert');
const webserver = NodeServer.moduleResolver.getModule('nodeserver');

var constructorCalled = false;
var lastAddRoutesArgument = false;
var portPassed = false;
const express = Express();

describe('nodeserver.js tests', function () {

    beforeEach(function(){
        class MockAppController {
            constructor() {
                constructorCalled = true;
            }
            addRoutes(addRoutesArgument) {
                lastAddRoutesArgument = addRoutesArgument;
            }
        }
        NodeServer.moduleResolver.setModule('controllers/app', {AppController: MockAppController});
        webserver.listen = function(port) {
            portPassed = port
        };

    });
    it('should have a default export that is an express object', function () {
        assert.strictEqual(Function.prototype.toString.call(webserver), Function.prototype.toString.call(express));
    });

    it('should expose a start function', function() {
        assert.strictEqual(typeof webserver.start, 'function');
    });

    it('should create a new AppController', function() {
        webserver.start(1338);
        assert(constructorCalled);

    });

    it('should call addRoutes, passing the webserver object in', function() {
        webserver.start(1338);
        assert.strictEqual(lastAddRoutesArgument, webserver);

    });


    it('should call listen, passing the port in', function() {

        webserver.start(1338);
        assert.strictEqual(portPassed, 1338);

    });

});
