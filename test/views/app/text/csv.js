const assert = require('assert');
let View;

require('../../../mocharootsuite');

describe('app text/csv view tests', function () {

    let view;

    before(function(){
        View = NodeServer.moduleResolver.getModule('views/app/text/csv').View;
    });

    beforeEach(function(){
        view = new View({config: {name: "foo", version: "bar", port: 31337}});
    });

    it('should call res.send from render', function () {


        let called = false;

        const mockResponse = {
            send: function () {
                called = true;
            }
        };

        view.render(mockResponse);
        assert(called);
    });

    it('should pass through "name" and "version" from object, should strip "port"', function () {

        let called = false;
        let lastData = null;

        const mockResponse = {
            send: function (data) {
                called = true;
                lastData = data;
            }
        };

        view.render(mockResponse);
        assert.strictEqual("name,version\nfoo,bar", lastData);
    });

});
