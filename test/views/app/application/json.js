const assert = require('assert');
let View;

require('../../../mocharootsuite');

describe('app application/json view tests', function () {

    let view;

    before(function(){
        View = NodeServer.moduleResolver.getModule('views/app/application/json').View;
    });

    beforeEach(function() {
        view = new View({config: { name: "foo", version: "bar", port: 31337}});
    });

    it('should call res.json from render', function () {

        let called = false;

        const mockResponse = {
            json: function () {
                called = true;
            }
        };

        view.render(mockResponse);
        assert(called);
    });

    it('should pass through "name" and "version" from object, should strip "port"', function () {

        let called = false;
        let lastJson = null;

        const mockResponse = {
            json: function (json) {
                called = true;
                lastJson = json;
            }
        };

        view.render(mockResponse);
        assert.deepStrictEqual(lastJson, {name: "foo", version: "bar"});
    });

});
